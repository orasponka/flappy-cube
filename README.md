# Flappy Cube
![](https://gitlab.com/orasponka/flappy-cube/-/raw/main/title.png) <br> <br>
Flappy Cube is simple Flappy Bird copy that i made just for fun. <br>
Made with Python and Pygame.

## How to install?
Make sure that you have installed python and pygame 
### Linux
```
git clone https://gitlab.com/orasponka/flappy-cube.git
cd flappy-cube
python game.py
```
### Windows
Just download this repo as zip. Then open File Explorer and open Downloads folder and extract fip file that you just downloaded. Then open folder that came with that zip file and double click flappycube.bat
## How to build?
### Arch Linux
Make sure that you have installed base-devel
```
git clone https://gitlab.com/orasponka/flappy-cube.git
cd flappy-cube
makepkg
sudo pacman -U *.zst
```