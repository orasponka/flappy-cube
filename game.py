import pygame
import threading
import os
from random import randint
from time import sleep

pygame.init()
pygame.font.init()
pygame.mixer.init()

width, height = 700, 500
p_width, p_height = 52, 52
fps = 60
bg_color = [(193,193,193), (207,207,207)]
p_color = (136, 189, 167)
o_color = (30, 30, 30)
next_o_color = (50, 50, 50)
o_width = 35
o_gap = 114
c_size = 20
c_color = (116, 169, 147)

font = pygame.font.get_default_font()
score_font = pygame.font.Font(os.path.join('Fonts', 'upheaval.ttf'), 20)
end_font = pygame.font.Font(os.path.join('Fonts', 'upheaval.ttf'), 40)
title_font = pygame.font.Font(os.path.join('Fonts', 'karma-future.ttf'), 100)
menu_font = pygame.font.Font(os.path.join('Fonts', 'upheaval.ttf'), 35)
menu_small_font = pygame.font.Font(os.path.join('Fonts', 'upheaval.ttf'), 20)
menu_medium_font = pygame.font.Font(os.path.join('Fonts', 'upheaval.ttf'), 25)

score_sound = pygame.mixer.Sound(os.path.join('Sounds', 'score.mp3'))
hit_sound = pygame.mixer.Sound(os.path.join('Sounds', 'hit.wav'))
wind_sound = pygame.mixer.Sound(os.path.join('Sounds', 'wind.ogg'))

window = pygame.display.set_mode((width, height))
pygame.display.set_caption("Flappy Cube")

highscore = 0
path = ""
parentdir = ""
folder = ""
filename = "highscore.txt"

if os.name == "nt":
    path = "Data"
    folder = "Data"
elif os.name == "posix":
    folder = "flappy-cube"
    path = os.path.expanduser("~") + "/.cache/flappy-cube/"
    parentdir = os.path.expanduser("~") + "/.cache"
else:
    path = "Data"
    folder = "Data"

if not os.path.exists(path) and os.name == "posix":
    dirpath = os.path.join(parentdir, folder)
    os.mkdir(dirpath)
    f = open(os.path.join(path,filename), "w")
    f.write(str(0))
    f.close()
elif not os.path.exists(path) and os.name == "nt":
    os.mkdir(folder)
    f = open(os.path.join(path,filename), "w")
    f.write(str(0))
    f.close()

def getHighScore():
    f = open(os.path.join(path, filename), "r")
    return int(f.read())

def writeHighScore(newHighScore):
    f = open(os.path.join(path, filename), "w")
    f.write(str(newHighScore))
    f.close()

highscore = getHighScore()

def gradientBg():
    target_rect = pygame.Rect(0,0,width,height)
    colour_rect = pygame.Surface( ( 2, 2 ) )
    pygame.draw.line( colour_rect, bg_color[0],  ( 0,0 ), ( 0,1 ) )
    pygame.draw.line( colour_rect, bg_color[1], ( 1,0 ), ( 1,1 ) )
    colour_rect = pygame.transform.smoothscale( colour_rect, ( target_rect.width, target_rect.height ) )
    window.blit( colour_rect, target_rect )

def game():
    clock = pygame.time.Clock()
    run = True

    player = pygame.Rect(150 - p_width / 2, height / 2 - p_height / 2, p_width, p_height)
    velocity = 0
    acceleration = 0.30

    score = 0
    global highscore

    obstacles = []
    lasto = ""
    scoreget = False

    cubes = []

    menu_bool = False

    next_obstacle = ""

    start_time = pygame.time.get_ticks()

    def spawn_obstacles(obstacles):
        def spawn():
            o_height = height / 2 + randint(-120, 120)
            obstacle = pygame.Rect(width + o_width / 2, o_height, o_width, height)
            obstacles.append(obstacle)

            obstacle2 = pygame.Rect(width + o_width / 2, 0, o_width, o_height - o_gap)
            obstacles.append(obstacle2)

            spawn_obstacles(obstacles)

        t = threading.Timer(1.80, spawn)
        t.start()
    spawn_obstacles(obstacles)

    def spawn_cubes(cubes):
        def spawn():
            if randint(1,4) == randint(1,4):
                cube = pygame.Rect(width + c_size / 2 + 180, randint(c_size, height - c_size), c_size, c_size)
                cubes.append(cube)
            spawn_cubes(cubes)

        t = threading.Timer(1.80, spawn)
        t.start()
    spawn_cubes(cubes)

    while run:
        clock.tick(fps)
        for o in obstacles:
            if o.x < 0 - o_width:
                obstacles.remove(o)
            elif o.colliderect(player):
                run = False
                hit_sound.play()
        if player.y > height or player.y + p_height < 0:
            run = False
            wind_sound.play()
        if len(obstacles) > 0:
            if not scoreget and obstacles[0].x < player.x:
                scoreget = True
                score += 1
                lasto = obstacles[0]
                score_sound.play()
            elif obstacles[0] != lasto:
                scoreget = False

        if len(obstacles) > 0:
            for o in obstacles:
                if o.x > player.x:
                    next_obstacle = o
                    break

        for c in cubes:
            if c.x + c_size < 0:
                cubes.remove(c)
            elif c.colliderect(player):
                cubes.remove(c)
                score += 3
                score_sound.play()

        if score > highscore:
            highscore = score
            writeHighScore(highscore)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
                quit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if pygame.mouse.get_pressed()[0]:
                    velocity = -4.75
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE or event.key == pygame.K_UP or event.key == pygame.K_w:
                    velocity = -4.75
                if event.key == pygame.K_ESCAPE:
                    run = False
                    menu_bool = True

        if run:
            player.y += velocity
            velocity += acceleration
            gradientBg()
            pygame.draw.rect(window, p_color, player)
            for o in obstacles:
                o.x -= 3
                if o == next_obstacle:
                    pygame.draw.rect(window, next_o_color, o)
                elif len(obstacles) > 0 and not next_obstacle == "":
                    if o == obstacles[obstacles.index(next_obstacle) + 1] and len(obstacles) > 0:
                        pygame.draw.rect(window, next_o_color, o)
                    elif not o == next_obstacle:
                        pygame.draw.rect(window, o_color, o)
                elif not o == next_obstacle:
                    pygame.draw.rect(window, o_color, o)

            for c in cubes:
                c.x -= 3
                pygame.draw.rect(window, c_color, c)

            score_text = score_font.render("Score: " + str(score), 1, (240, 240, 240))
            window.blit(score_text, (4, 2))

            highscore_text = score_font.render("Highscore: " + str(highscore), 1, (240, 240, 240))
            window.blit(highscore_text, (4, 3 + score_text.get_height()))

            time = pygame.time.get_ticks() - start_time
            minutes = str(int(time/60000)).zfill(2)
            seconds = str(int((time%6000)/1000)).zfill(2)
            time_string = "%s:%s" % (minutes, seconds)
            timer_text = score_font.render(time_string, 1, (240,240,240))
            window.blit(timer_text, (width - 4 - timer_text.get_width(), 2))

            pygame.display.update()
        elif not run and not menu_bool:
            gradientBg()
            pygame.draw.rect(window, p_color, player)
            for o in obstacles:
                if o == next_obstacle:
                    pygame.draw.rect(window, next_o_color, o)
                elif len(obstacles) > 0 and not next_obstacle == "":
                    if o == obstacles[obstacles.index(next_obstacle) + 1] and len(obstacles) > 0:
                        pygame.draw.rect(window, next_o_color, o)
                    elif not o == next_obstacle:
                        pygame.draw.rect(window, o_color, o)
                elif not o == next_obstacle:
                    pygame.draw.rect(window, o_color, o)
            for c in cubes:
                pygame.draw.rect(window, c_color, c)
            end_text = end_font.render("Your score was " + str(score), 1, (240, 240, 240))
            window.blit(end_text, (width / 2 - end_text.get_width() / 2, height / 2 ))
            pygame.display.update()
            sleep(3)
            game()
        elif not run and menu_bool:
            menu()

def menu():
    clock = pygame.time.Clock()
    run = True

    global highscore

    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    run = False
                if event.key == pygame.K_ESCAPE:
                    run = False
                    pygame.quit()
                    quit()
        if run:
            gradientBg()
            title = title_font.render("Flappy Cube",1,(10,10,10))
            window.blit(title, (width / 2 - title.get_width() / 2, height / 2 - title.get_height()))
            press_space_text = menu_font.render("Press Space to start", 1, (10, 10, 10))
            window.blit(press_space_text, (width / 2 - press_space_text.get_width() / 2, height / 2 + 30))
            highscore_text = menu_medium_font.render("Current Highscore: " + str(highscore), 1, (10, 10, 10))
            window.blit(highscore_text, (width / 2 - highscore_text.get_width() / 2, height / 2 + 70))
            creator = menu_small_font.render("Oras Pönkä 2022-2023", 1, (10, 10, 10))
            window.blit(creator, (2, height - creator.get_height() - 2))
            pygame.display.update()
        elif not run:
                game()

if __name__ == "__main__":
    menu()
